# redis-dockerfile

We need to have a /data dir be mounted as sshfs directory. So, we can not change privileges on mounted dir.
That is why we can run redis as root user only, to have write access to mounted /data. 


The corrected dockerfile from 
&#34;redis/5.0/alpine/Dockerfile&#34;
https://github.com/docker-library/redis/blob/dcc0a2a343ce499b78ca617987e8621e7d31515b/5.0/alpine/Dockerfile

###Make image

    docker build --tag=show4me/redis:5.0.4-alpine .
    docker push show4me/redis:5.0.4-alpine

###Creating service

    SERVICE_NAME=redis-test; \
    DOCKER_NET_NAME=betanet; \
    IMAGE_NAME=show4me/redis:5.0.2-alpine; \
    docker service create \
    --limit-memory=300m \
    --name=${SERVICE_NAME}  \
    --hostname=${SERVICE_NAME}  \
    --network=${DOCKER_NET_NAME} \
    --mount type=bind,source=/etc/localtime,target=/etc/localtime:ro \
    --mount type=bind,source=/mnt/backup-server/tmp1,target=/data \
    ${IMAGE_NAME}